package test.spring.portlet;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.permission.LayoutPermissionUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * @author zod
 */
@Controller
@RequestMapping("VIEW")
public class TestSpringPortletViewController {

	
	
	@RenderMapping
	public String view(RenderRequest request, RenderResponse response) {
		
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		
		try {
			if (LayoutPermissionUtil.contains(themeDisplay.getPermissionChecker(), themeDisplay.getLayout(), ActionKeys.UPDATE)) {
				return "update";
			}
		} catch (PortalException e) {
			// Ignored
		}
		
		return "view";
	}
}